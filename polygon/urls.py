from django.conf.urls import *

from polygon.views import IndexView, CheckDotInPolygonView

urlpatterns = [
    url(r'^is_in_polygon/', CheckDotInPolygonView.as_view()),
    url(r'^$', IndexView.as_view()),
]
