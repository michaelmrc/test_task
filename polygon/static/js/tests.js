
// Проверка точек, совпадающих с вершинами полигона
function testVertices() {
    POLYGON.forEach(function(point) {
        sendPointToCheck(SEND_POINT_URL, point, function(response) {
            drawPoint(response);
        });
    });
}

// Проверка точек на ребрах полигона
function testEdges() {
    var pointsOnEdges = POLYGON.slice(0, -1).map(function(currentPoint, index) {
        var nextPoint = POLYGON[index+1];
        var pointX = (currentPoint[0] + nextPoint[0]) / 2;
        var pointY = (currentPoint[1] + nextPoint[1]) / 2;
        return [pointX, pointY]
    })

    pointsOnEdges.forEach(function(point, index) {
        sendPointToCheck(SEND_POINT_URL, point, function(response) {
            drawPoint(response);
        });
    });
}

// Проверка точек, когда трассирующий луч проходит через вершины полигона
function testEdgeOrVertexOnRay() {
    var horizontalEdgesX = [300, 400, 500, 600, 700 ,750];
    var horizontalEdgesY = [];
    var pointsOnEdges = POLYGON.map(function(point) {
        if (horizontalEdgesY.indexOf(point[1]) === -1) {
            horizontalEdgesY.push(point[1]);
        }
    })

    horizontalEdgesY.forEach(function(pointY) {
        horizontalEdgesX.forEach(function(pointX) {
            sendPointToCheck(SEND_POINT_URL, [pointX, pointY], function(response) {
                drawPoint(response);
            });
        });
    });
}
