
var POLYGON = [
    [100, 50],
    [215, 80],
    [265, 80],
    [285, 180],
    [315, 80],
    [455, 80],
    [500, 100],
    [715, 30],
    [315, 330],
    [640, 240],
    [700, 540],
    [600, 540],
    [620, 500],
    [520, 400],
    [420, 540],
    [360, 540],
    [320, 580],
    [220, 380],
    [120, 480],
    [180, 330],
    [20, 200],
    [100, 50]
];

var CANVAS_WIDTH = 800;
var CANVAS_HEIGHT = 600;
var SEND_POINT_URL = '/is_in_polygon/';

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        return document.cookie.split(name + '=')[1].split(';')[0];
    }
    return cookieValue;
}

// отправка данных для проверки
function sendPointToCheck(url, point, success, fail) {
    var csrfToken = getCookie('csrftoken');

    var headers = new Headers({
        'Content-Type': 'x-www-form-urlencoded',
        'X-CSRFToken': csrfToken
    });

    var bodyData = {
        "polygon": POLYGON,
        "point": point
    };

    var params = {
        headers: headers,
        method: 'POST',
        body: JSON.stringify(bodyData),
        credentials: 'include'
    };

    if (window.fetch) {
        return fetch(url, params)
            .then(function (response) {
                if (response.status !== 200) {
                    // fail
                } else return response.json();
            }).then(function (data) {
                success(data)
            })
    }
}

function getCanvasContext() {
    var canvas = document.getElementById('canvas-main');
    if (canvas && canvas.getContext) {
        return canvas.getContext("2d");
    } else {
        console.log('No canvas element with this id');
    }
}

function renderPolygon() {
    var context = getCanvasContext();

    context.canvas.width = CANVAS_WIDTH;
    context.canvas.height = CANVAS_HEIGHT;

    context.beginPath();

    POLYGON.forEach(function(vertex, index) {
        if (index === 0) context.moveTo(vertex[0], vertex[1]);
        else context.lineTo(vertex[0], vertex[1]);
    });

    context.strokeStyle = '#123';
    context.stroke();
};

function clearCanvas() {
    var context = getCanvasContext();
    context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    renderPolygon();
};

function drawPoint(data) {
    p = data['point'];
    var canvasContext = getCanvasContext();

    canvasContext.beginPath();
    canvasContext.arc(p[0], p[1], 5, 0, 2 * Math.PI, false);

    // По результату запроса зеленая или красная точка
    if (data['in_polygon'])
        canvasContext.fillStyle = "#73AD21";
    else
        canvasContext.fillStyle = "#FF0000";

    canvasContext.fill();
}

function canvasClickEvent(event) {
    var pointX = event.pageX - this.offsetLeft;
    var pointY = event.pageY - this.offsetTop;
    
    sendPointToCheck(SEND_POINT_URL, [pointX, pointY], function(response) {
        drawPoint(response);
    });

}
