import json

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView, View


# запись csrf token в cookie
@method_decorator(ensure_csrf_cookie, name='dispatch')
class IndexView(TemplateView):
    """Отображение canvas с полигоном"""
    template_name = 'polygon/index.html'


class CheckDotInPolygonView(View):
    """Проверка расположения точки внутри полигона"""

    def is_in_polygon(self, point, polygon):
        """Проверка расположения точки внутри полигона"""
        point_x, point_y = point
        in_polygon = False

        # точка находится в вершине полигона
        if point in polygon:
            return True

        for i, vertex in enumerate(polygon):
            # ребро полигона
            xp, yp = vertex
            xp_prev, yp_prev = polygon[i-1]

            # точка находится на горизонтальном ребре полигона
            if yp == point_y == yp_prev and \
                    (xp < point_x < xp_prev or xp_prev < point_x < xp):
                return True

            # ребро полигона по обе стороны от луча
            if (yp <= point_y < yp_prev) or (yp_prev <= point_y < yp):
                # точка пересечения трассирующего луча и ребра полигона
                intersection_x = (xp_prev - xp) * (point_y - yp) / (yp_prev - yp) + xp

                # точка находится на ребре полигона
                if point_x == intersection_x:
                    return True

                # луч пересекает ребро полигона
                if point_x > intersection_x:
                    in_polygon = not in_polygon
        return in_polygon

    def post(self, request):
        data = json.loads(request.body.decode('utf-8'))
        in_polygon = self.is_in_polygon(point=data['point'],
                                        polygon=data['polygon'])
        data.update(in_polygon=in_polygon)
        return JsonResponse(data)
